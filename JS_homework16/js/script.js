document.getElementById("changeThemeBtn").addEventListener("click", changeTheme);
document.getElementById("theme").href = `${localStorage.activeTheme}`;

if (localStorage.activeTheme == undefined) {
      localStorage.setItem('activeTheme', 'css/style.css');
}

function changeTheme() {
    let themeCSS = document.getElementById("theme");
    switch (localStorage.activeTheme) {
        case 'css/style.css':
        localStorage.setItem('activeTheme', 'css/style-theme.css')
        themeCSS.href = localStorage.activeTheme;
        break;
        case 'css/style-theme.css':
        localStorage.setItem('activeTheme', 'css/style.css')
        themeCSS.href = localStorage.activeTheme;
        break;
    }
}

