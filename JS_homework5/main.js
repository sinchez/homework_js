let oldObj = {
	number: 12345,
	string: 'some text',
	object: {
		number: 42123,
		string: 'text some',
	},
	array: [4,1,2,1,2,'string'],
}
console.log(oldObj);
let copyObj = (obj) => {
	let newObj = {};
	for(let key in obj){
		if(typeof obj[key] !=="object" || obj[key] === null){
			newObj[key] = obj[key];
		}else{
			newObj[key] = copyObj(obj[key]);
		}
	}
	return newObj;
}

let newObj = copyObj(oldObj);
console.log(newObj);